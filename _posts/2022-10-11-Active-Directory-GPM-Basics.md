---
layout: post
title:  "Active Directory GPM Basics"
date:   2022-10-11 16:55:14 -0300
categories: Post
---


NOTE:
To setup AD LAB you will need.
LAB
	Windows 10 Enterprise * 2
	Windows server 2019

Requirements:
	 16 GB RAM minimum

And checkout TCM video by Heath Adams.





To deploy different policies for each OU individually

Windows manages such policies through Group Policy Objects (GPO). GPOs are simply a collection of settings that can be applied to OUs. GPOs can contain policies aimed at either users or computers, allowing you to set a baseline on specific machines and identities.

To configure GPOs, you can use the Group Policy Management tool, available from the start menu:


To configure Group Policies, you first create a GPO under Group Policy Objects and then link it to the GPO where you want the policies to apply. As an example, you can see there are some already existing GPOs in your machine:
{:refdef: style="text-align: center;"}
![AD GPM](/blogs/img/GPM_-1.png)
{: refdef}


We can see in the image above that 3 GPOs have been created. From those, the Default Domain Policy and RDP Policy are linked to the thm.local domain as a whole, and the Default Domain Controllers Policy is linked to the Domain Controllers OU only. Something important to have in mind is that any GPO will apply to the linked OU and any sub-OUs under it. For example, the Sales OU will still be affected by the Default Domain Policy.


GPO distribution

GPOs are distributed to the network via a network share called SYSVOL, which is stored in the DC. All users in a domain should typically have access to this share over the network to sync their GPOs periodically. The SYSVOL share points by default to the C:\Windows\SYSVOL\sysvol\ directory on each of the DCs in our network.

EXAMPLE:
TO Create some GPOs for our local machine

As part of our new job, we have been tasked with implementing some GPOs to allow us to:

    Block non-IT users from accessing the Control Panel.
    Make workstations and servers lock their screen automatically after 5 minutes of user inactivity to avoid people leaving their sessions exposed.


Restrict Access to Control Panel

We want to restrict access to the Control Panel across all machines to only the users that are part of the IT department. Users of other departments shouldn't be able to change the system's preferences.

Let's create a new GPO called Restrict Control Panel Access and open it for editing. Since we want this GPO to apply to specific users, we will look under User Configuration for the following policy:

Restricting access to control panel

{:refdef: style="text-align: center;"}
![AD GPM](/blogs/img/GPM-2.png)
{: refdef}
Notice we have enabled the Prohibit Access to Control Panel and PC settings policy.

Once the GPO is configured, we will need to link it to all of the OUs corresponding to users who shouldn't have access to the Control Panel of their PCs. In this case, we will link the Marketing, Management and Sales OUs by dragging the GPO to each of them:
{:refdef: style="text-align: center;"}
![AD GPM](/blogs/img/GPM__3_.png)
{: refdef}


Auto Lock Screen GPO

For the first GPO, regarding screen locking for workstations and servers, we could directly apply it over the Workstations, Servers and Domain Controllers OUs we created previously.

While this solution should work, an alternative consists of simply applying the GPO to the root domain, as we want the GPO to affect all of our computers. Since the Workstations, Servers and Domain Controllers OUs are all child OUs of the root domain, they will inherit its policies.

Note: You might notice that if our GPO is applied to the root domain, it will also be inherited by other OUs like Sales or Marketing. Since these OUs contain users only, any Computer Configuration in our GPO will be ignored by them.

Let's create a new GPO, call it Auto Lock Screen, and edit it. The policy to achieve what we want is located in the following route:

{:refdef: style="text-align: center;"}
![AD GPM](/blogs/img/GPM__4_.png)
{: refdef}

{:refdef: style="text-align: center;"}
![AD GPM](/blogs/img/GPM__5_.png)
{: refdef}

