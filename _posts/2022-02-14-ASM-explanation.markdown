---
layout: post
title:  "Assembly with Hello World!"
date:   2022-02-14 15:32:14 -0000
categories: New Learning
---
A normal "Hellow World" Program can be seen below, which we will explain further.

```
; hello_world.asm
;Intel_syntax

global  _start

section .text :
_start:`
		mov eax, 0x4             ; write syscall
		mov ecx, 1               ; use stdout as the fd
		mov exc, message         ; message as  the buffer
		mov edx, message_length  ; supply length
		int 0x80                 ; syscall
		
		mov eax ,1               ; exit code
		mov ebx, 0               
		int 0x80 
		
section.data:
	message: db "Hello World!", 0xA
	message_length equ $-message

```


For any ASM program, we need to define section, generally started with '.' , **text** and **data**.


# .text #
It has all program code and instructions.

.text consist of sys call represented by numbers that interacts with low level kernel.
	syscall locate by
		```
		"locate unistd_32.h"
		```

	example exit 1, read 3, write 4, open 5

*mov eax, 0x4  ;    write syscall*

	we can checkout the write man page. 

ssize_t write(int **_fd_** , const void **_buf_**, size_t  **_count_**);
	
	fd here is file decriptor, 1 for stdout
	buf = for message.
	count = length to print.
	ssize_t = return value send to eax

so register **eax**, has no of arguments
**ebx**, file descriptor
**ecx** is the buf
**edx** is the count

for more info on system calls, [Assembly_System_Calls](https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm#:~:text=There%20are%20six%20registers%20that,ESI%2C%20EDI%2C%20and%20EBP)


As from url [Data_Registers](https://stackoverflow.com/questions/53355682/are-the-data-registers-eax-ebx-ecx-and-edx-interchangeable ) 
in general, it does not matter what registers you use for what values. In your own code, you are free to use almost all registers (except for such as registers as `esp`) for whatever purpose you want as long as you don't interact with other peoples code.

after setting up all values and registers we call for syscall
using 	`int 0x80` 

	 ; Exit code
	   mov eax,1 
	   mov ebx, 0 
	   int 80h




=================================================================




## .data ##

.data has all variables and labels, used by program.


section.data:
	**message**: db "Hello World!", 0xA

		message=variable
		db = is data bytes
		0xA = \n

**message_length equ $-message**

		$- = length in nasm





## EXECUTING

```
$ nasm -f elf32 -o hello.o hello_world.asm  #hello_world.asm our file
```
now we have .o file

```
$ld -m elf_i386 -o hello_world hell.0=o
```


## Further_words
Global directive is NASM specific used for exporting symbols in a code to where it points in the object code generated.
Here, in example our mark  \_start us global  so its name is added in the object code(hello.o). the linker(ld) then can read that symbol in the object code, and its value so that i can mark as an entry point in the executable.

If you want to use a different entry point name other than `_start` (which is the default), you can specify `-e` parameter to ld like:


```
ld -e my_entry_point -o out a.o
```
