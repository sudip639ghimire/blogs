---
layout: post
title:  "Active Directory Basics"
date:   2022-10-11 14:45:14 -0300
categories: Post
---


Direcotry service developed by microsoft to manage Windows domain networks.

Stores information related to object, such as computers, users, printers, etc

Authenticates using kerberos tickets.
	Non-windows devices also authenticate to active directory via RADIUS or LDAP.

It is most common IAM service in the world.

==Physical Active DIrectory Components:==


1. Domain Controller:
it is a server  with the AD DS(domain service) server role installed that has specifically been promoted to a domain controller.
	-it has directory store having all information
	-provides authentication and authorization services.
	-replicates updates  to other domain controller in domain and forest.
	-allow administrative access to manages user accounsts and network resources.

2. AD DS Data Store
it contains the databases files, and process that store and manage directory information for users, services, and applications
The AD DS data store:
		consists of the NTds.dit file
		is stored by default in the %SystemRoot%\\NTDS folder on all domain controllers.
		is accessible only through the domain controller processses and protocols.

Note: The NTds.dit file is very sensitive. So grab this file as it contains everything stored in AD data like users, objects, groups, passwords hashes.


This two components are things we will try to compromise. 


==Logical AD Components==

AD DS Schema:
- Defines every type of objects that can be stored in the directory
- Enforces rules regarding object creation and configuration.

eg
	User, computer are example of class object types
	display name is attribute attached to an object.


Domains:
	used to group and manage objects in an organizations.

Domains:
	an administratie boundary for applying policies to group of objects.
	similar to domain controllers.
	But its a domain, that can function as domain controllers, it is for managing components in their boundary. Like TLD in dns like .com or .edu


TREES:
	domain tree is a heirarchy of domains in AD DS.
	View it like a TLD and Authoritive NS.
for example domains emeea.contoso.com and na.contoso.com falls under domain contoso.com
parent domain -> multiple child domain -> child domains
has two-way trust with domains in link, and share namespaces

FORESTS:
Collection of trees.

like collection of .com and .edu and has some trust. 
they share a common schema.
share common configuration partition.
enables trust between all domains in the forest.


Organization Unit (Ous):
They are  AD containers that can contain users, groups, computers, and other Ous.
	 represent organization heirarchically and logically.
	 manage collection of obhects in a consistent way


TRUSTS:
it provides mechanism for users to gain access to resources in another domain.


Types of trust
- Directional We have trust one domain to another domain.
- Transitive trust everything that a trusted domains trust.


OBJECTS:
Its going to be inside OUs.
example user -> enables network resource access for a user.
, group -> for simple administrative accesss control.
computers -> enables auth and auditing of computer access to resources.
Share folders ->
printers ->
contact ->

==Summary==

We have domains -> group and manage objects in a org

Tree -> multiple domain

Forest -> multiple tress.

Inside of its tree domain, has Ous which consists of objects.

so Ous are smallest unit with in domain can holds users, group and computer togets..


Ous + Ous -> domain