---
layout: post
title:  "Keyg3n"
date:   2022-02-13 10:17:00 
categories: Crackmes.one
---

Source:
[crackmes.one](https://crackmes.one/crackme/5da31ebc33c5d46f00e2c661)


Description: 
Easy, you just need to figure out the logic behind key validation. this should be fairly easy even with an ugly debugger. i'm new here, so the difficulty ranking could be a little off.


Comments:
```
C/C++
Platform: Unix/linux etc.
Difficulty: 1.2 Rate!
Quality: 4.3 Rate!
Arch: x86-64
```



Lets dive right into it with `FILE` and later with `radare2`:

getting `pdf @ main`, then `pdf @ validate_key` function:

```
[0x7f174a47e2df]> pdf @ sym.validate_key
            ; CALL XREF from main @ 0x55f3d0bdc1af
┌ 59: sym.validate_key (int64_t arg1);
│           ; var int64_t var_4h @ rbp-0x4
│           ; arg int64_t arg1 @ rdi
│           0x55f3d0bdc1ee      55             push rbp
│           0x55f3d0bdc1ef      4889e5         mov rbp, rsp
│           0x55f3d0bdc1f2      897dfc         mov dword [var_4h], edi ; arg1
│           0x55f3d0bdc1f5      8b4dfc         mov ecx, dword [var_4h]
│           0x55f3d0bdc1f8      baad0acb1a     mov edx, 0x1acb0aad
│           0x55f3d0bdc1fd      89c8           mov eax, ecx
│           0x55f3d0bdc1ff      f7ea           imul edx
│           0x55f3d0bdc201      c1fa07         sar edx, 7
│           0x55f3d0bdc204      89c8           mov eax, ecx
│           0x55f3d0bdc206      c1f81f         sar eax, 0x1f
│           0x55f3d0bdc209      29c2           sub edx, eax
│           0x55f3d0bdc20b      89d0           mov eax, edx
│           0x55f3d0bdc20d      69c0c7040000   imul eax, eax, 0x4c7
│           0x55f3d0bdc213      29c1           sub ecx, eax
│           0x55f3d0bdc215      89c8           mov eax, ecx
│           0x55f3d0bdc217      85c0           test eax, eax
│       ┌─< 0x55f3d0bdc219      7507           jne 0x55f3d0bdc222
│       │   0x55f3d0bdc21b      b801000000     mov eax, 1
│      ┌──< 0x55f3d0bdc220      eb05           jmp 0x55f3d0bdc227
│      │└─> 0x55f3d0bdc222      b800000000     mov eax, 0
│      │    ; CODE XREF from sym.validate_key @ 0x55f3d0bdc220
│      └──> 0x55f3d0bdc227      5d             pop rbp
└           0x55f3d0bdc228      c3             ret





```


Lets crack this using this; using references from different assembly sites:



### **Note
### before jumping I want to you to know this things 
( I have known this while goin through validate_key code )


here since 32 bit register is used when performing `imul edx`
eax = eax * edx
	`r/m32 x EAX -> EDX:EAX r/m[16|32] x reg[16|32] -> reg|16|32]`
this means that overflow value will be written in `EDX`, its values is replaced.







### So Comming Back to track:

```
argi==edi

ecx = var_4h = arg1
edx = 0x1acb0aad 	  	
eax = ecx		                  ; arg1
eax = eax * edx		              ; 	imul edx ; eax = arg1 * 0x1acb0aad
sar edx, 7		                  ; edx /= 2^7 shift righ preserve sign; 
eax = ecx
sar  eax, 0x1f		              ; eax /= (2^31); 31 count; thus eax = 0;
edx = edx -eax 
eax = edx  
eax =eax * 0x4c7 ;      eax = (edx) * 0x4c7; **FFFFFE53**
ecx = ecx - eax  ;      ecx = ecx - (edx * 0x4c7)
eax = ecx;

test = eax, eax 	; 
jne abcd:
eax,1									
jmp abcd:
abcd:
mov eax = 0
return

```


Here, test  check if eax 0 or not, if 0, zeroflag is on:
if not not equal that means zf is 0  

so on caller function cmparing is done with test which like below:

```
if cmp eax, 1
	if not 1 
	return 0;

else 
 return 1

```

### **NOTE
with test, for example
`test cl, cl   `; set ZF to 1 if cl == 0
											jnz 0x804f430  ; jump if ZF == 0, if cl is 1.

`cmp eax, 1; 
							set zf if eax == 1;
		cmp eax, 2; set zf if eax ==2 ;

check if eax 0 or not, if 0 zeroflag is on
if not not equal that means zf not 0  jump return ;				; Conditional Jump with NOT




## EXAMPLE

Still its not relevant with this, so lets test with a number as input, 
this program accepts decimal value but for easiness, I will carry out with hex

 since, thers number `0x4c7` and `test`  lets begin with it:
 ```
 eax = ecx = arg1 = 0x4c7
 edx = 0x1acb0aad

 imul edx results in eax * edx = 0x(4c7 * 1acb0aad)
 it results in 80 00 00 00 7B = EDX:EAX                as per note
 stored as edx = 0x80 and eax = 7b

 now sar edx by 7 results 
 edx =1

 sar eax 31 results
 eax = 0

 eax= edx = 1-0 = 1

 imul eax = 1 * 0x4c7
 ecx = ecx -eax = 0
 eax =ecx =0
 test eax, eax
 return 1 on 0
 
 
 ```

 This was okay as It seems we need multiple of 0x4c7 or 1223:

 if we try with multiple of 0x4c7

 ```
 arg 1 = 2 * 0x4c7 = 0x983

 which results in imul makind edx = 0x100
 
 sar edx,7 results edx to be 2 

 so at last its 0x98e - 2 * 0x4c7*
 ```



## Solution
We find that:

![Keyg3n](/blogs/img/keygenme.png)

## GHIDRA
It took 2 days but yeah its good to go through it, now we look at code using GHIDRA.

- find main function:
	-over there you can see `validate_key`
![Keyg3n](/blogs/img/keygenme_GHIDRA.png)



## Validate_key
Simply with GHIDRA we can see the end functions that consist modular division of `0x4c7`
this validate our results.

![Keyg3n](/blogs/img/keygenme_GHIDRA2.png)



## KEYGEN


```
x is some random integer
python -c "print(1223*x)" | ./keyg3nme

```


## CONCLUSION

Though we could had simply used GHIDRA(or other tools), but we decided to go through the code directly, which took some time, but surely would be benefecial later.
