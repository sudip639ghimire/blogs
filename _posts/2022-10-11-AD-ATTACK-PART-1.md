---
layout: post
title:  "Active Directory Attacks - 1"
date:   2022-10-11 18:20:14 -0300 
categories: Post
---


 Techniques for enumerating AD::
	 
	The AD snap-ins of the Microsoft Management Console.
	The net commands of Command Prompt.
	The AD-RSAT cmdlets of PowerShell.
	Bloodhound.

In AD credentials plays a huge roles. If you can get your hands on one then you can pivot and exploit the whole Network.

Techniques to recover AD credentials in this network:

    NTLM Authenticated Services
    LDAP Bind Credentials
    Authentication Relays
    Microsoft Deployment Toolkit
    Configuration Files


New Technology LAN Manager (NTLM) is the suite of security protocols used to authenticate users' identities in AD. NTLM can be used for authentication by using a challenge-response-based scheme called NetNTLM. This authentication mechanism is heavily used by the services on a network


### Brute-force Login Attacks

example You have been provided with a list of usernames discovered during a red team OSINT exercise. The OSINT exercise also indicated the organisation's initial onboarding password, which seems to be "Changeme123". Although users should always change their initial password, we know that users often forget. We will be using a custom-developed script to stage a password spraying against the web application hosted at this URL: http://ntlmauth.za.tryhackme.com.


We can run the script using the following command:

`python ntlm_passwordspray.py -u <userfile> -f <fqdn> -p <password> -a <attackurl>`

We provide the following values for each of the parameters:
```

    <userfile> - Textfile containing our usernames - "usernames.txt"
    <fqdn> - Fully qualified domain name associated with the organisation that we are attacking - "za.tryhackme.com"
    <password> - The password we want to use for our spraying attack - "Changeme123"
    <attackurl> - The URL of the application that supports Windows Authentication - "http://ntlmauth.za.tryhackme.com"
```

Using these parameters, we should get a few valid credentials pairs from our password spraying attack.




### AUTHENTICATION RELAYS
#### NetNTLM authentication used by SMB.

SERVER MESSAGE BLOCK 

The Server Message Block (SMB) protocol allows clients (like workstations) to communicate with a server (like a file share). In networks that use Microsoft AD, SMB governs everything from inter-network file-sharing to remote administration. Even the "out of paper" alert your computer receives when you try to print a document is the work of the SMB protocol.

two different exploits for NetNTLM authentication with SMB:

- Since the NTLM Challenges can be intercepted, we can use offline cracking techniques to recover the password associated with the NTLM Challenge. However, this cracking process is significantly slower than cracking NTLM hashes directly.
- We can use our rogue device to stage a man in the middle attack, relaying the SMB authentication between the client and server, which will provide us with an active authenticated session and access to the target server. 




### USING RESPONDER:

Responder allows us to perform Man-in-the-Middle attacks by poisoning the responses during NetNTLM authentication, tricking the client into talking to you instead of the actual server they wanted to connect to. On a real LAN, Responder will attempt to poison any  Link-Local Multicast Name Resolution (LLMNR),  NetBIOS Name Servier (NBT-NS), and Web Proxy Auto-Discovery (WPAD) requests that are detected. On large Windows networks, these protocols allow hosts to perform their own local DNS resolution for all hosts on the same local network. Rather than overburdening network resources such as the DNS servers, hosts can first attempt to determine if the host they are looking for is on the same local network by sending out LLMNR requests and seeing if any hosts respond. The NBT-NS is the precursor protocol to LLMNR, and WPAD requests are made to try and find a proxy for future HTTP(s) connections.

Since these protocols rely on requests broadcasted on the local network, our rogue device would also receive these requests. Usually, these requests would simply be dropped since they were not meant for our host. However, Responder will actively listen to the requests and send poisoned responses telling the requesting host that our IP is associated with the requested hostname. By poisoning these requests, Responder attempts to force the client to connect to our AttackBox. In the same line, it starts to host several servers such as SMB, HTTP, SQL, and others to capture these requests and force authentication. 

Intercepting NetNTLM Challenge

`Sudo responder -I tun0`

Responder will now listen for any LLMNR, NBT-NS, or WPAD requests that are coming in. 


If we were using our rogue device, we would probably run Responder for quite some time, capturing several responses. Once we have a couple, we can start to perform some offline cracking of the responses in the hopes of recovering their associated NTLM passwords. If the accounts have weak passwords configured, we have a good chance of successfully cracking them. Copy the NTLMv2-SSP Hash to a textfile. We will then use the password list provided in the downloadable files for this task and Hashcat in an attempt to crack the hash using the following command:
```
hashcat -m 5600 <hash file> <password file> --force
```





Relaying the Challenge

In some instances, however, we can take this a step further by trying to relay the challenge instead of just capturing it directly. This is a little bit more difficult to do without prior knowledge of the accounts since this attack depends on the permissions of the associated account. We need a couple of things to play in our favour:

```
SMB Signing should either be disabled or enabled but not enforced. When we perform a relay, we make minor changes to the request to pass it along. If SMB signing is enabled, we won't be able to forge the message signature, meaning the server would reject it.
    The associated account needs the relevant permissions on the server to access the requested resources. Ideally, we are looking to relay the challenge and response of an account with administrative privileges over the server, as this would allow us to gain a foothold on the host.
    Since we technically don't yet have an AD foothold, some guesswork is involved into what accounts will have permissions on which hosts. If we had already breached AD, we could perform some initial enumeration first, which is usually the case.
```


{:refdef: style="text-align: center;"}
![AD_RELAY_CHALLENGE](/blogs/img/Relay_challenge.png)
{: refdef}
