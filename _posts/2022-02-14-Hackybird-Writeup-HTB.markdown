---
layout: post
title:  "HTB-HACKYBIRD"
date:   2022-02-14 22:12:00 
categories: Hackthebox
---

Source: [HTB](https://www.hackthebox.com/)

Description:
For this you need to learn about CheatEngine. You just need to figure out the address that uses to store the score. 

### Learn CheatEngine
[CheatEngine](https://www.cheatengine.org/) is a free and open-source memory scanner/debugger. It was new for me, and the great source of tutorial was through the Cheat Engine Tutorial (x64) in the `Help` tabs itself.


## Lets Dive.
Firstly, I tried the program using Ghidra and IDA free, which took a long time to load, and consists of lots of game logic. 
So, After Learning about cheat Engine, we come up to check the variable or adressess acessing score number, which would be integer for sure.
Thus based on the game and score, there must be some bypass value to win the game or get the flag. Therefore, lets us first search for the  variable on Cheat Engine.

* ### Score VARIABLE:
Though you can see only one image it took me atleast 10 - 20 tries per single opening to pinpoint the variable. 
For that as taught in tutorial, launch Cheat Engine and HackyBird, and make cheat engine look at Hackybird process.

{:refdef: style="text-align: center;"}
![hacky](/blogs/img/hacky.png)
{: refdef}

After playing and scoring a lots of 1, I manage to score 5, thus was able to search for the address than contains the value `5`. Its `002E0A1C`, also everytime running different process addresses gets changes, only pattern I noticed it `....A1C` at last. Double clicking the address help to get in the box, and again for validating I played and scored 1 which was shown in the same address. Thus we landed on Correct address.

{:refdef: style="text-align: center;"}
![Hacky](/blogs/img/hacky1.png)
{: refdef}

As from the tutorial, we can access what addressses access or writes to its value, and play the game. So on playing on scoring a score provides how the score is updated and is compared to some value. So we can see our value is at `[esi + 00000094]`, which is increased by 1, and finally after finishing it compares it to `0x3E7` which is `999` in decimal. Also a thing is there is a counter variable that sums the score.

* ### Further
If we look at cmp addresss and search it in IDA, we will find the same comparision but in more visible manner.
So we can guess that it checks to `999`.

So lets go back to beginning, check on the address under `Active`, and edit the value to 999. And play.

### Observation and Conclusion:
While editing and playing, initially I couldnt score 1 intially, or sometimes score more than 1. As we also have counter variable that sums up the score, the game ends by showing some garbages on the scree. 
So trying again and again the thing I find is, the score should be exact 1000. So if its `999`, score `1`. If its `998` score `2`. Or you can put `1000` and die on 1. Remember once your counter varaible goes beyond `1000`, game halts, and you will need to find the address again, making the process redundant.

{:refdef: style="text-align: center;"}
![Hacky Flag](/blogs/img/flag_hackybird.png)
{: refdef}
Here, is the flag after like 10's of time.


## BEST OF LUCK
