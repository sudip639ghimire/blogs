---
layout: page
title: About
permalink: /about/
---

{:refdef: style="text-align: center;"}
![My LOGO]({{ site.baseimg }}/blogs/img/ghost.png)
{: refdef}

Hello world, It's me Lay-D3ad (simply meaning laying down in bed and doing things).

This writesup covers range of security topics and challenges from websites:
* [Crackmes.one](https://crackmes.one/) 
* [HackTheBox](https://www.hackthebox.com/)
* [TryhackMe](https://tryhackme.com/)
* [Pwnable.kr](http://pwnable.kr/)

Most of our focus will be on Reverse Engineering, Cryptography, and later will be extended to  Exploitation/Web/Misc/Forensics in above platforms.


## HOW I STARTED?
Since the time I learnt about virus I have great enthusiasm for Security. But not knowing how to learn this things, I have always indulged in theories only. With CTF's, and available sites everything changed, its been since 2020 I started learning things and still carrying on. This is a blog for my CTF Writeups, who want to start into Reverse Engineering and Cryptography.

Anyway, if you wanna know more, feel free to follow me on Twitter, or see my resume here!

Also, you can contact me anytime at my email on the link in the footer.


## Want to start building Blogs?
Interested in the theme, or want to read/link to the .md files directly? You can find this repository’s source on Github. 
You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](https://jekyllrb.com/)

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jekyll" %} /
[minima](https://github.com/jekyll/minima)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)


## Last Words
For any confusions or recommendations, you can contact me anytime at my email on the link below.
Suggestions are appreciated.
